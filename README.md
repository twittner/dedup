Dedup
=====

Utilities supporting single instance storage of files.

Contributing
------------

If you want to contribute to this project please read the file
CONTRIBUTING first.
