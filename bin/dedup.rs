#[macro_use]
extern crate clap;
extern crate dedup;
extern crate rustc_serialize;

use clap::{Arg, App, SubCommand};
use dedup::{Config, Index};
use rustc_serialize::hex::FromHex;
use std::io::Write;
use std::path::Path;

fn main() {
    let args = App::new("dedup")
        .version("0.1.0")
        .about("Single instance storage commands.")
        .subcommand(SubCommand::with_name("init")
            .about("Create index database.")
            .arg(Arg::with_name("index").short("i").long("index")
                 .help("Where to store index metadata.")
                 .required(true)
                 .takes_value(true)))
        .subcommand(SubCommand::with_name("add")
            .about("Add directory tree to index.")
            .arg(Arg::with_name("index").short("i").long("index")
                 .help("Where to store index metadata.")
                 .required(true)
                 .takes_value(true))
            .arg(Arg::with_name("source").short("s").long("source")
                 .help("Directory tree to add.")
                 .required(true)
                 .takes_value(true))
            .arg(Arg::with_name("target").short("t").long("target")
                 .help("Optional target directory to copy to.")
                 .takes_value(true)))
        .subcommand(SubCommand::with_name("remove")
            .about("Remove file from index.")
            .arg(Arg::with_name("index").short("i").long("index")
                 .help("Where to store index metadata.")
                 .required(true)
                 .takes_value(true))
            .arg(Arg::with_name("hash").short("h").long("hash")
                 .help("Hash to remove.")
                 .required(true)
                 .takes_value(true))
            .arg(Arg::with_name("delete").short("d").long("delete")
                 .help("Delete file matching hash from file system."))
            .arg(Arg::with_name("verbose").short("v").long("verbose")
                 .help("Print file name matching hash.")))
        .subcommand(SubCommand::with_name("print")
            .about("Print index.")
            .arg(Arg::with_name("index").short("i").long("index")
                 .help("Index directory.")
                 .required(true)
                 .takes_value(true)))
        .get_matches();

    match args.subcommand() {
        ("init", Some(cmd)) => {
            let index = cmd.value_of_os("index").map(Path::new).unwrap();
            Index::open(Config::default(), &index).unwrap();
        }
        ("add", Some(cmd)) => {
            let index  = cmd.value_of_os("index").map(Path::new).unwrap();
            let source = cmd.value_of_os("source").map(Path::new).unwrap();
            let target = cmd.value_of_os("target").map(Path::new);
            Index::open(Config::default(), &index)
                .unwrap()
                .add(source, target, |org, dup| println!("{} = {}", org, dup))
                .unwrap();
        }
        ("remove", Some(cmd)) => {
            let index    = cmd.value_of_os("index").map(Path::new).unwrap();
            let delete   = cmd.is_present("delete");
            let verbose  = cmd.is_present("verbose");
            let hash_str = cmd.value_of("hash").unwrap();
            let hash_vec = hash_str.from_hex().unwrap();
            let mut hash = [0; 32];
            (&mut hash[..]).write_all(&hash_vec).unwrap();
            let cfg = Config::default().create(false);
            Index::open(cfg, &index)
                .unwrap()
                .remove(&hash, delete, verbose)
                .unwrap();
        }
        ("print", Some(cmd)) => {
            let index = cmd.value_of_os("index").map(Path::new).unwrap();
            Index::open(Config::default(), &index)
                .unwrap()
                .print_index()
                .unwrap()
        }
        _ => println!("{}", args.usage())
    }
}
