use crypto::digest::Digest;
use crypto::sha2::Sha256;
use error::{DedupError, DedupResult};
use lmdb_rs::{Environment, DbFlags, DbHandle};
use lmdb_rs::core::MdbError;
use rustc_serialize::hex::ToHex;
use std::fs::{self, File};
use std::io::{BufRead, BufReader};
use std::path::Path;
use walkdir::WalkDir;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Config {
    pub create: bool,
    pub map_size: usize
}

const DEFAULT_CONFIG: Config = Config {
    create:   true,
    map_size: 1073741824
};

impl Config {
    pub fn default() -> Config { DEFAULT_CONFIG }

    pub fn create(self, x: bool) -> Config {
        Config { create: x, .. self }
    }

    pub fn map_size(self, x: usize) -> Config {
        Config { map_size: x, .. self }
    }
}

pub struct Index {
    env:    Environment,
    handle: DbHandle
}

impl Index {
    /// Open index database (create if it does not exist).
    pub fn open(c: Config, p: &Path) -> DedupResult<Index> {
        let e = Environment::new()
            .map_size(c.map_size as u64)
            .autocreate_dir(c.create)
            .open(p, 0o777)?;
        let d = e.get_default_db(DbFlags::empty())?;
        Ok(Index { env: e, handle: d })
    }

    /// Add directory tree to index and optionally copy it's files to
    /// the given target location.
    pub fn add<F>(&self, source: &Path, target: Option<&Path>, f: F) -> DedupResult<()>
      where F: Fn(&str, &str) {
        match target {
            Some(t) => self.copy(source, t, f),
            None    => self.insert(source, f)
        }
    }

    fn insert<F>(&self, source: &Path, fun: F) -> DedupResult<()>
      where F: Fn(&str, &str) {
        let txn = self.env.new_transaction()?;
        {
            let db = txn.bind(&self.handle);
            for entry in WalkDir::new(source) {
                let entry = entry?;
                if !entry.file_type().is_file() {
                    continue
                }
                let name = &*entry.path().to_string_lossy();
                let hash = &sha256(File::open(entry.path())?)?[..];
                match db.insert(&hash, &name) {
                    Ok(_)                    => (),
                    Err(MdbError::KeyExists) => fun(db.get(&hash)?, name),
                    Err(e)                   => return Err(DedupError::Db(e))
                }
            }
        }
        txn.commit().map_err(From::from)
    }

    fn copy<F>(&self, from: &Path, to: &Path, fun: F) -> DedupResult<()>
      where F: Fn(&str, &str) {
        let txn = self.env.new_transaction()?;
        {
            let db = txn.bind(&self.handle);
            for entry in WalkDir::new(from) {
                let entry = entry?;
                if !entry.file_type().is_file() {
                    continue
                }
                let hash = &sha256(File::open(entry.path())?)?[..];
                let dest = to.join(entry.path().strip_prefix(from).unwrap());
                match db.insert(&hash, &&*dest.to_string_lossy()) {
                    Ok(_) => {
                        match dest.parent() {
                            None    => (),
                            Some(p) => fs::create_dir_all(p)?
                        }
                        fs::copy(entry.path(), &dest)?;
                    }
                    Err(MdbError::KeyExists) => {
                        let name = &*entry.path().to_string_lossy();
                        fun(db.get(&hash)?, name)
                    }
                    Err(e) => return Err(DedupError::Db(e))
                }
            }
        }
        txn.commit().map_err(From::from)
    }

    /// Remove the given sha256 hash from index, optionally removing the
    /// corresponding file.
    pub fn remove(&self, hash: &[u8; 32], rm: bool, verbose: bool) -> DedupResult<bool> {
        let txn = self.env.new_transaction()?;
        let res = {
            let db = txn.bind(&self.handle);
            match db.get(&&hash[..]) {
                Ok(name) => {
                    if verbose {
                        println!("Removing \"{}\".", &name);
                    }
                    db.del(&&hash[..])?;
                    if rm {
                        fs::remove_file(&(name: &str))?
                    }
                    Ok(true)
                }
                Err(MdbError::NotFound) => {
                    if verbose {
                        println!("Nothing matches \"{}\".", hash.to_hex())
                    }
                    Ok(false)
                }
                Err(e) => Err(DedupError::Db(e))
            }
        };
        txn.commit()?;
        res
    }

    /// Print every key value pair of this index.
    pub fn print_index(&self) -> DedupResult<()> {
        let rdr = self.env.get_reader()?;
        let db  = rdr.bind(&self.handle);
        for kv in db.iter()? {
            println!("{} -> {}", (kv.get_key(): &[u8]).to_hex(), kv.get_value(): &str);
        }
        Ok(())
    }
}

/// Calculate SHA-256 hash of the given file.
pub fn sha256(f: File) -> DedupResult<[u8; 32]> {
    let mut r = BufReader::new(f);
    let mut h = Sha256::new();
    loop {
        let n = {
            let b = r.fill_buf()?;
            if b.len() == 0 {
                break
            }
            h.input(&b);
            b.len()
        };
        r.consume(n)
    }
    let mut hash = [0; 32];
    h.result(&mut hash);
    Ok(hash)
}
