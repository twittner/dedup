#![feature(question_mark, type_ascription)]

extern crate lmdb_rs;
extern crate crypto;
extern crate rustc_serialize;
extern crate walkdir;

pub mod error;
pub mod index;

pub use index::{Config, Index, sha256};
