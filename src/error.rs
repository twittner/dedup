use lmdb_rs::core::MdbError;
use std::error::Error;
use std::fmt;
use std::io;
use walkdir;

pub type DedupResult<T> = Result<T, DedupError>;

#[derive(Debug)]
pub enum DedupError {
    Io(io::Error),
    Db(MdbError),
    Fs(walkdir::Error)
}

impl fmt::Display for DedupError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match *self {
            DedupError::Io(ref e) => write!(f, "i/o: {:?}", e),
            DedupError::Db(ref e) => write!(f, "lmdb: {:?}", e),
            DedupError::Fs(ref e) => write!(f, "file: {:?}", e)
        }
    }
}

impl Error for DedupError {
    fn description(&self) -> &str {
        "DedupError"
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            DedupError::Io(ref e) => Some(e),
            DedupError::Db(ref e) => Some(e),
            DedupError::Fs(ref e) => Some(e)
        }
    }
}

impl From<io::Error> for DedupError {
    fn from(e: io::Error) -> DedupError {
        DedupError::Io(e)
    }
}

impl From<MdbError> for DedupError {
    fn from(e: MdbError) -> DedupError {
        DedupError::Db(e)
    }
}

impl From<walkdir::Error> for DedupError {
    fn from(e: walkdir::Error) -> DedupError {
        DedupError::Fs(e)
    }
}
